const MongoClient = require('mongodb')
require('dotenv').config({silent:true})
const url =process.env.URL
const {taskObject,agentObject}=require('./config')


MongoClient.connect(url, { useNewUrlParser: true }, async function (err, client) {
  try {
    const db = client.db('jobQueue');
    const tasks = db.collection('tasks');
    const agents = db.collection('agenttasks');
    // const taskat=await tasks.find({}).toArray()
    const migratedScript = await migrate(tasks, agents);
    console.log(migratedScript)
  } catch (error) {
    console.log(error)
  }
});

const migrate = async (tasks, agents) => {
  return new Promise(async (resolve, reject) => {
    try {
      await tasks.find({ assigneeName: { $exists: false } }).forEach(async task => {
        const { fullName } = await agents.findOne({ agentId: task.agentId })
        await tasks.findOneAndUpdate({ _id: task._id }, { $set: { assigneeName: fullName } })
      });
      resolve({ success: true })
    } catch (error) {
      reject({ success: false })
    }
  })
}
 