const taskObject = {
    "taskType": "refund.approval",
    "taskEntity": "merchant",
    "taskEntityIdentifier": "merchantPayformanceId",
    "taskEntityIdentifierValue": "MID-1-128",
    "status": "open",
    "agentId": "5d0122694333f702163950fc",
    "payload": {
      "mode": "test",
      "provider": {
        "method": "card",
        "type": "mpgs"
      },
      "merchant": {
        "refundInfo": {
          "provider": {
            "method": "card",
            "type": "mpgs"
          },
          "merchantOrderId": "5d079c2b1ecc08002bc452fe",
          "currency": "EGP",
          "amount": "111111",
          "merchantPayformanceId": "MID-1-128",
          "orderId": "d3ccf5f3-a44b-42b0-a9d8-390c8acf0ab5"
        },
        "auth": {
          "userType": "merchant",
          "userId": "5cea7f153ad24a003dc704f1"
        },
        "merchantPayformanceId": "MID-1-128",
        "merchantOrderId": "5d079c2b1ecc08002bc452fe"
      },
      "transaction": {
        "currency": "EGP",
        "amount": "111111"
      }
    },
  
    "timeline": [],
    "isActive": true,
    "creationDate": Date.now(),
    "attributes": [],
  }
  const agentObject = {
    "agentId": "5d0122694333f702163950fc",
    "agentEmail": "agent5@elements-ft.com",
    "fullName": "agent5",
    "isActive": true,
    "isRobined": true,
    "creationDate": Date.now(),
    "isSupervisor": false,
  }
module.exports={
    taskObject,
    agentObject
}